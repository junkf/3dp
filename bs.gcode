M109 S195.000000

G21        ;metric values
G90        ;absolute positioning
M82        ;set extruder to absolute mode
M107       ;start with the fan off
G28 X0 Y0  ;move X/Y to min endstops
G28 Z0     ;move Z to min endstops
G1 Z15.0 F6000 ;move the platform down 15mm
G92 E0                  ;zero the extruded length
G1 F200 E3              ;extrude 3mm of feed stock
G92 E0                  ;zero the extruded length again


G1 F6000

G0 F6000 X43.681 Y44.418 Z0.300

G1 F1200 X44.482 Y43.662 E0.13738
G1 X44.847 Y43.323 E0.19951
G1 X45.818 Y42.600 E0.35050
G1 X46.870 Y41.930 E0.50606
G1 X47.256 Y41.698 E0.56223
G1 X48.244 Y41.194 E0.70057
G1 X49.441 Y40.668 E0.86365
G1 X50.689 Y40.238 E1.02828
G1 X51.754 Y39.981 E1.16493
G1 X52.733 Y39.786 E1.28943
G1 X54.185 Y39.921 E1.47132
G1 X55.113 Y40.039 E1.58799
G1 X56.499 Y40.608 E1.77486


M107
G1 F6000 E7440.46357
G0 X54.969 Y63.344 Z64.948
